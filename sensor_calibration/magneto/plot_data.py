import matplotlib.pyplot as plt
import numpy as np

#arr shape of (3,1)
def cube_root(arr):
    print("inside")
    temp_arr = list(map(lambda a: a**2, list(arr)))
    print(temp_arr)
    return (np.sum(np.array(temp_arr)))**(1/2) 

b = np.array([-0.007352, 0.018360, 0.016137])

A = np.array([[2.046439, -0.081541, -0.015170], [-0.081541, 2.091299, 0.011992], [0.015170, 0.011992, 2.217662]])

mag_x_y_data = []
mag_x_z_data = []
mag_y_z_data = []

mag_x_y_data_cal = []
mag_x_z_data_cal = []
mag_y_z_data_cal = []

#format of data "num1 num2  num3"
with open('mag_data_for_calib_x_y.txt') as f:
    while True:
        line = f.readline()
        if not line: 
            break
        #print(line)
        line_ = line.replace("\n", "")
        #line__ = line.replace(" ","  ") 
        line_splitted = line_.split(" ")
        print(line_splitted)
        mag_x_y_data.append([float(line_splitted[0]), float(line_splitted[1])])
        temp_arr = np.array([float(line_splitted[0])-b[0], float(line_splitted[1])-b[1], float(line_splitted[3])-b[2]]).reshape((3,1))
        # print(temp_arr.shape)
        mat_mul = np.matmul(A, temp_arr)
        print("mat_mul =", mat_mul)
        print("total magnetic field =", cube_root(mat_mul))
        mag_x_y_data_cal.append([mat_mul[0], mat_mul[1]])

with open('mag_data_for_calib_x_z.txt') as f:
    while True:
        line = f.readline()
        if not line: 
            break
        #print(line)
        line_ = line.replace("\n", "")
        #line__ = line.replace(" ","  ") 
        line_splitted = line_.split(" ")
        # print(line_splitted)
        mag_x_z_data.append([float(line_splitted[0]), float(line_splitted[3])])
        temp_arr = np.array([float(line_splitted[0])-b[0], float(line_splitted[1])-b[1], float(line_splitted[3])-b[2]]).reshape((3,1))
        # print(temp_arr.shape)
        mat_mul = np.matmul(A, temp_arr)
        # print("mat_mul.shape =", mat_mul.shape)
        print("total magnetic field =", cube_root(mat_mul))
        mag_x_z_data_cal.append([mat_mul[0], mat_mul[2]])

with open('mag_data_for_calib_y_z.txt') as f:
    while True:
        line = f.readline()
        if not line: 
            break
        #print(line)
        line_ = line.replace("\n", "")
        #line__ = line.replace(" ","  ") 
        line_splitted = line_.split(" ")
        # print(line_splitted)
        mag_y_z_data.append([float(line_splitted[1]), float(line_splitted[3])])
        temp_arr = np.array([float(line_splitted[0])-b[0], float(line_splitted[1])-b[1], float(line_splitted[3])-b[2]]).reshape((3,1))
        # print(temp_arr.shape)
        mat_mul = np.matmul(A, temp_arr)
        # print("mat_mul.shape =", mat_mul.shape)
        print("total magnetic field =", cube_root(mat_mul))
        mag_y_z_data_cal.append([mat_mul[1], mat_mul[2]])

# print(mag_x_data)
# print(mag_y_data)
# print(mag_z_data)

mag_x_y_data = np.array(mag_x_y_data)
mag_x_z_data = np.array(mag_x_z_data)
mag_y_z_data = np.array(mag_y_z_data)

mag_x_y_data_cal = np.array(mag_x_y_data_cal)
mag_x_z_data_cal = np.array(mag_x_z_data_cal)
mag_y_z_data_cal = np.array(mag_y_z_data_cal)

print(mag_x_y_data.shape, mag_x_y_data_cal.shape)
print(mag_x_y_data_cal)
print(mag_y_z_data.shape)


fig, axes = plt.subplots(1)#, figsize = (20, 16))
plt.plot(mag_x_y_data[:, 0], mag_x_y_data[:, 1], "r*", label="uncalibrated X-Y")
plt.plot(mag_x_y_data_cal[:, 0], mag_x_y_data_cal[:, 1], "b*", label="calibrated X-Y")
plt.legend()
plt.xlabel("X axis magnetometer data")
plt.ylabel("Y axis magnetometer data")
plt.title("X-Y magnetometer figure")
plt.axis('equal')

fig, axes = plt.subplots(1)#, figsize = (20, 16))
plt.plot(mag_x_z_data[:, 0], mag_x_z_data[:, 1], "r*", label="uncalibrated X-Z")
plt.plot(mag_x_z_data_cal[:, 0], mag_x_z_data_cal[:, 1], "b*", label="calibrated X-Z")
plt.legend()
plt.xlabel("X axis magnetometer data")
plt.ylabel("Z axis magnetometer data")
plt.title("X-Z magnetometer figure")
plt.axis('equal')

fig, axes = plt.subplots(1)#, figsize = (20, 16))
plt.plot(mag_y_z_data[:, 0], mag_y_z_data[:, 1], "r*", label="uncalibrated Y-Z")
plt.plot(mag_y_z_data_cal[:, 0], mag_y_z_data_cal[:, 1], "b*", label="calibrated Y-Z")
plt.legend()
plt.xlabel("Y axis magnetometer data")
plt.ylabel("Z axis magnetometer data")
plt.title("Y-Z magnetometer figure")
plt.axis('equal')

#---------------------------------------------
# import numpy as np 
# # import matplotlib.pyplot as plt 
 
# angle = np.linspace( 0 , 2 * np.pi , 150 ) 
 
# radius = 0.4
 
# x = radius * np.cos( angle ) 
# y = radius * np.sin( angle ) 
 
# figure, axes = plt.subplots( 1 ) 
 
# axes.plot( x, y ) 
# axes.set_aspect( 1 ) 
 
# plt.title( 'Parametric Equation Circle' ) 

plt.show()