import orhelper  
from orhelper import FlightDataType, FlightEvent
import matplotlib.pyplot as plt 
import numpy as np 
import time 
from tqdm import tqdm 
import pandas as pd 
import os 

# Create output directory 
sim_out_path = "./output"
if(os.path.exists("./output") ==  False): 
    os.makedirs("./output")

# set a standard random seed to be able to produce the same parameters if for example a rocket part changes
np.random.seed(13) 

# Specify simulation limits and parameters  
windspeed_lims = np.array([0,10]) # windspeed limits - m/s
windspeed_std_lims = np.array([0,3]) # windspeed standard deviation limits - m/s 
turbint_lims = np.array([0,0.5]) # turbulence intensity limits - float between [0,1]
winddir_lims = np.array([0,10]) # wind direction limits - angle in degrees
#lat_lims = np.array([]) # launch site latitude limits - TBD
#long_lims = np.array([]) # launch site longitude limits - TBD
alt_lims = np.array([0,1100]) # launch site altitude limits - m
rodlen_lims = np.array([4,5.5]) # launch rod length limits - m
rodang_lims = np.array([1,5]) # launch rod angle limits - angle in degrees

winddir_lims = np.radians(winddir_lims)
rodang_lims = np.radians(rodang_lims)

sim_index = 0 # choose simulation if there are multiple simulations e.g. different motors
sim_samples = 500 # total number of simulations 

# Generate simulation input data 
windspeed = np.random.uniform(low = windspeed_lims[0], high = windspeed_lims[1], size = (1,sim_samples))
windspeed_std = np.random.uniform(low = windspeed_std_lims[0], high = windspeed_std_lims[1], size = (1,sim_samples))
turbint = np.random.uniform(low = turbint_lims[0], high = turbint_lims[1], size = (1,sim_samples))
winddir = np.random.uniform(low = winddir_lims[0], high = winddir_lims[1], size = (1,sim_samples))
#lat = np.random.uniform(low = lat_lims[0], high = lat_lims[1], size = (1,sim_samples))
#long = np.random.uniform(low = long_lims[0], high = long_lims[1], size = (1,sim_samples))
alt = np.random.uniform(low = alt_lims[0], high = alt_lims[1], size = (1,sim_samples))
rodlen = np.random.uniform(low = rodlen_lims[0], high = rodlen_lims[1], size = (1,sim_samples))
rodang = np.random.uniform(low = rodang_lims[0], high = rodang_lims[1], size = (1,sim_samples))

print("Simulation input data generated.")
print("Starting simulations")

start_time = time.time()

with orhelper.OpenRocketInstance() as instance: 
    orh = orhelper.Helper(instance)

    # Load rocket model document
    doc = orh.load_doc("./rocket_models/Cronos.ork")

    # Select which simulation to work on  
    sim = doc.getSimulation(sim_index) # simulation number from the list of simulations in the .ork file 


    for i in tqdm(range(sim_samples)) : 

        sim.getOptions().setWindSpeedAverage(windspeed[0,i])
        sim.getOptions().setWindSpeedDeviation(windspeed_std[0,i])
        sim.getOptions().setWindTurbulenceIntensity(turbint[0,i])
        sim.getOptions().setWindDirection(winddir[0,i])
        sim.getOptions().setLaunchAltitude(alt[0,i])
#        sim.getOptions().setLaunchLatitude(lat[0,i])
#        sim.getOptions().setLaunchLongitude(long[0,i])
        sim.getOptions().setLaunchRodAngle(rodang[0,i])
        sim.getOptions().setLaunchRodLength(rodlen[0,i])

        orh.run_simulation(sim)

        results = orh.get_timeseries(sim, [FlightDataType.TYPE_TIME, FlightDataType.TYPE_ALTITUDE, FlightDataType.TYPE_ACCELERATION_TOTAL])
        df = pd.DataFrame(data = results)
#        df.rename(columns={"FlightDataType.TYPE_TIME" : "Time", "FlightDataType.TYPE_ALTITUDE" : "Altitude", "FlightDataType.TYPE_ACCELERATION_TOTAL" : "Acc_Tot"})
        pathname = "./output/sim_" + str(i) + ".csv"
        df.to_csv(pathname,header=["Time","Altitude","Acc_tot"])


end_time = time.time()
runtime = end_time - start_time 
print("Runtime = " + str(runtime) + "sec")
